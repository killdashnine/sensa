import {Kuzzle, WebSocket} from 'kuzzle-sdk';

const kuzzle = new Kuzzle(new WebSocket('localhost'));

kuzzle.on('networkError', error => {
    console.error('Network Error:', error);
});

const run = async () => {
    try {
        await kuzzle.connect();

        // Subscribes to document notifications using the above filter
        const roomId = await kuzzle.realtime.subscribe('sensa', 'events', {
            equals: { type: 'VIDEO' }
        }, (notification) => {
            console.log('---> ', notification);
            if (notification.type === 'document' && notification.action === 'create') {
                const {
                    _source: event,
                    _id: eventId
                } = notification.result;

                console.log(`---> New event ${event.title} with id ${eventId} has VIDEO type.`);
                // kuzzle.realtime.unsubscribe(roomId);
                // kuzzle.disconnect();
            }
        });

        console.log('Successfully subscribed to document notifications!');
    } catch (error) {
        console.error(error.message);
    }
};

run().then(() => { console.log('done'); });
