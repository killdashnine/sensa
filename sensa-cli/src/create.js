import {Kuzzle, WebSocket} from 'kuzzle-sdk';

const kuzzle = new Kuzzle(new WebSocket('localhost'));

kuzzle.on('networkError', error => {
    console.error('Network Error:', error);
});

const run = async () => {
    try {
        // Connects to the Kuzzle server
        await kuzzle.connect();

        // Creates a document
        const event = {
            type: 'VIDEO',
            title: 'Камера "Во дворе"',
            description: "Замечено движение в области",
            priority: 'major',
            // videoUrl: "http://path_to_some_video.com/uri",
            // createdAt: new Date(),
        };

        await kuzzle.document.create('sensa', 'events', event);

        console.log('New document successfully created!');
    } catch (error) {
        console.error(error.message);
    } finally {
        kuzzle.disconnect();
    }
};

run().then(() => { console.log('done'); });
