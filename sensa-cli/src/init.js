import {Kuzzle, WebSocket} from 'kuzzle-sdk';

const kuzzle = new Kuzzle(new WebSocket('localhost'));

kuzzle.on('networkError', error => {
    console.error('Network Error:', error);
});

const run = async () => {
    try {
        // Connects to the Kuzzle server
        await kuzzle.connect();

        // Creates an index
        await kuzzle.index.create('sensa');

        // Creates a collection
        await kuzzle.collection.create('sensa', 'events');

        console.log('sensa/events ready!');
    } catch (error) {
        console.error(error.message);
    } finally {
        kuzzle.disconnect();
    }
};

run().then(() => { console.log('done'); });
