import React, {useContext} from "react";
import ConfigContext from "../context/ConfigContext";
import {observer, useLocalStore} from "mobx-react";
import EventStore from "../state/Event/EventStore";
import EventService from "../state/Event/EventService";
import useSubscription from "../util/useSubscription";
import EventStoreContext from "../context/EventStoreContext";
import EventServiceContext from "../context/EventServiceContext";

function EventProvider({children}) {
    const config = useContext(ConfigContext);
    const events = useLocalStore(() => new EventStore());
    const service = useLocalStore(() => new EventService({config, events}));

    useSubscription(service);

    return (<EventStoreContext.Provider value={events}>
        <EventServiceContext.Provider value={service}>
            {children}
        </EventServiceContext.Provider>
    </EventStoreContext.Provider>);
}

export default observer(EventProvider);
