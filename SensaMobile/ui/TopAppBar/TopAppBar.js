import React from 'react';
import {StyleSheet, Text, View} from "react-native";
import {Icon} from "react-native-elements";


export default function TopAppBar() {
    return (<View style={styles.container}>
        <View style={styles.iconWrapper}>
            <Icon name='chevron-left' color='white' size={36}/>
        </View>
        <Text style={styles.text}>История происшествий</Text>
    </View>);
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 16,
        backgroundColor: '#424242',
    },
    iconWrapper: {
        flexGrow: 0,
        paddingHorizontal: 16,
    },
    text: {
        flexGrow: 1,
        color: 'white',
        fontSize: 16,
    },
});

