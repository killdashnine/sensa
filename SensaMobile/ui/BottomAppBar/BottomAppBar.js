import React from 'react';
import {StyleSheet, View} from "react-native";
import {Icon} from "react-native-elements";


export default function BottomAppBar() {
    return (<View style={styles.container}>
        <Icon name='computer' color='#E2AD50' size={36}/>
        <Icon name='access-time' color='white' size={36}/>
        <Icon name='chat' color='#E2AD50' size={36}/>
        <Icon name='person' color='#E2AD50' size={36}/>
    </View>);
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingVertical: 16,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
    },
});

