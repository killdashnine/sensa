import React, {useCallback, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {Icon} from "react-native-elements";
import {observer} from "mobx-react";
import {DateTime} from 'luxon';

function EventCard({event, containerStyle}) {
    const [isExpanded, setExpanded] = useState(false);
    const isActuallyExpanded = event.type === 'VIDEO' && event.videoUrl && isExpanded;
    const createdAt = DateTime.fromMillis(event.createdAt);

    const handleCardPress = useCallback(() => setExpanded(isOpen => !isOpen), []);

    return (<TouchableOpacity onPress={handleCardPress}>
        <View style={containerStyle}>
            {isActuallyExpanded && <View style={styles.content}>
                {event.videoUrl
                    ? <Image source={{
                        uri: event.videoUrl,
                    }} style={{width: '100%', aspectRatio: 1}}/> : null}
                <View style={styles.videoControls}>
                    <View style={styles.controlsLeft}>
                        <Icon containerStyle={styles.controlIcon} name='photo-camera' color='#E2AD50' size={36}/>
                        <Icon containerStyle={styles.controlIcon} name='volume-up' color='#E2AD50' size={36}/>
                    </View>
                    <View style={styles.controlsCenter}>
                        <Icon containerStyle={styles.controlIcon} name='fast-rewind' color='#E2AD50' size={36}/>
                        <Icon containerStyle={styles.controlIcon} name='play-circle-outline' color='white' size={36}/>
                        <Icon containerStyle={styles.controlIcon} name='fast-forward' color='#E2AD50' size={36}/>
                    </View>
                    <View style={styles.controlsRight}>
                        <Icon containerStyle={styles.controlIcon} name='more-vert' color='#707070' size={36}/>
                    </View>
                </View>
            </View>}
            <View style={[styles.card, isActuallyExpanded && styles.cardExpanded]}>
                <View style={[
                    styles.indicator,
                    event.priority === 'minor' && styles.indicatorMinor,
                    event.priority === 'major' && styles.indicatorMajor,
                    isActuallyExpanded && styles.indicatorHidden]}/>
                <View style={[styles.header, isActuallyExpanded && styles.headerExpanded]}>
                    <Text style={styles.time}>{createdAt.toLocaleString(DateTime.TIME_24_SIMPLE)}</Text>
                    <View style={styles.labels}>
                        <Text style={[styles.title, isActuallyExpanded && styles.titleExpanded]}>{event.title}</Text>
                        {!isActuallyExpanded && <Text style={styles.description}>{event.description}</Text>}
                    </View>
                    {!isActuallyExpanded && <View style={styles.icon}>
                        <Icon name='play-circle-outline' color='white' size={36}/>
                    </View>}
                </View>
                {!isActuallyExpanded && <View style={[styles.indicator, styles.indicatorHidden]}/>}
            </View>
        </View>
    </TouchableOpacity>);
}

const styles = StyleSheet.create({
    content: {
        backgroundColor: '#424242',
        flex: 1,
    },
    videoControls: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        padding: 8,
    },
    controlsLeft: {
        flexGrow: 1,
        flexShrink: 0,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    controlsCenter: {
        position: 'absolute',
        flex: 1,
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 8,
    },
    controlsRight: {
        flexGrow: 1,
        flexShrink: 0,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    controlIcon: {
        margin: 8,
    },
    card: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    cardExpanded: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: 'transparent',
    },
    indicator: {
        width: 16,
        height: '100%',
    },
    indicatorMajor: {
        backgroundColor: '#FF275A',
    },
    indicatorMinor: {
        backgroundColor: '#E2AD50',
    },
    indicatorHidden: {
        backgroundColor: 'transparent',
    },
    header: {
        flex: 1,
        flexShrink: 0,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#424242',
    },
    headerExpanded: {
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
    },
    time: {
        paddingLeft: 8,
        paddingRight: 8,
        color: 'white',
    },
    labels: {
        flexGrow: 1,
        paddingLeft: 16,
    },
    title: {
        color: 'white',
        marginTop: 16,
        marginBottom: 8,
        fontWeight: 'bold',
        fontSize: 16,
    },
    titleExpanded: {
        marginTop: 8,
    },
    description: {
        color: 'white',
        marginTop: 8,
        marginBottom: 16,
        fontSize: 12,
    },
    icon: {
        flexShrink: 0,
        paddingHorizontal: 24,
    },
});

export default observer(EventCard);
