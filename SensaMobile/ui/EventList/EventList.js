import React, {useContext} from "react";
import EventStoreContext from "../../context/EventStoreContext";
import {observer} from "mobx-react";
import EventTypeSwitch from "../EventTypeSwitch/EventTypeSwitch";
import {ScrollView, StyleSheet, View} from "react-native";
import BottomAppBar from "../BottomAppBar/BottomAppBar";
import EventCard from "../EventCard/EventCard";
import {map} from 'lodash';


function EventList({BottomReserve}) {
    const events = useContext(EventStoreContext);

    return (<ScrollView>
        <EventTypeSwitch/>
        {/*<EventDivider/>*/}
        {map(events.byId, event =>
            <EventCard key={event.id} event={event} containerStyle={styles.eventCard}/>)}
        {BottomReserve && <BottomReserve/>}
        <View>
            <BottomAppBar/>
            <View style={styles.backgroundStub}/>
        </View>
    </ScrollView>);
}

const styles = StyleSheet.create({
    backgroundStub: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#2B2A28',
    },
    eventCard: {
        marginVertical: 4,
    },
});

export default observer(EventList);
