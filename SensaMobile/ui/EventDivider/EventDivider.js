import React from 'react';
import {StyleSheet, Text, View} from "react-native";


export default function EventDivider() {
    return (<View style={styles.container}>
        <View style={styles.lineWrapper}>
            <View style={styles.line}/>
        </View>
        <Text style={styles.text}>16.10.2019</Text>
        <View style={styles.lineWrapper}>
            <View style={styles.line}/>
        </View>
    </View>);
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    lineWrapper: {
        flexGrow: 1,
        paddingHorizontal: 16,
    },
    line: {
        height: 1,
        backgroundColor: '#E2AD50',
        width: '100%',
    },
    text: {
        flexGrow: 0,
        flexShrink: 0,
        color: '#E2AD50',
    },
});
