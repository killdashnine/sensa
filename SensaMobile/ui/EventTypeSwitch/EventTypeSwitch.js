import React from 'react';
import {StyleSheet, View} from "react-native";
import {Icon} from "react-native-elements";


export default function EventTypeSwitch() {
    return (<View style={styles.container}>
        <Icon name='date-range' color='#E2AD50' size={36}/>
        <Icon name='videocam' color='white' size={36}/>
        <Icon name='cloud-queue' color='#E2AD50' size={36}/>
        <Icon name='invert-colors' color='#E2AD50' size={36}/>
    </View>);
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingVertical: 8,
    },
});

