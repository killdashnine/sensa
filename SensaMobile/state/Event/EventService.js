import {action, observable} from "mobx";
import {assign} from "lodash";


export default class EventService {
    @observable config;
    @observable events;
    timeout = 2000;
    intervalId;

    constructor({config, events}) {
        assign(this, {config, events});
    }

    @action.bound
    async fetch() {
        try {
            const response = await fetch(this.config.serverUrl);
            const events = await response.json();
            for (let event of events) {
                this.events.upsert(event); // FIXME Deleted events will persist in memory store until force reload
            }
        } catch (error) {
            console.error(error);
        }
    }

    subscribe() {
        this.unsubscribe();
        this.intervalId = setInterval(this.fetch, this.timeout);
    }

    unsubscribe() {
        if (this.intervalId)
            clearInterval(this.intervalId);
    }
}
