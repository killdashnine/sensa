import {action} from "mobx";
import BaseStore from "../Base/BaseStore";
import Event from './Event';

export default class EventStore extends BaseStore {
    @action add(event) {
        if (!(event instanceof Event))
            event = new Event(event);

        return super.add(event);
    }
}
