import {action, observable} from "mobx";
import {assign, pick} from "lodash";

export default class Event {
    @observable id;
    @observable type;
    @observable title;
    @observable description;
    @observable videoUrl;
    @observable priority;
    @observable createdAt;

    constructor(event) {
        this.id = event.id;
        this.update(event);
    }

    @action update(event) {
        assign(this, pick(event, ['type', 'title', 'description', 'videoUrl', 'priority', 'createdAt']));
    }
}
