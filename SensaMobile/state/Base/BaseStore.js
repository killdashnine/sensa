import {action, observable} from "mobx";
import {forEach, merge} from "lodash";

export default class BaseStore {
    @observable byId = {};

    get(recordOrId) {
        return recordOrId ?
            typeof recordOrId === 'object'
                ? this.byId[recordOrId.id]
                : this.byId[recordOrId] || null
            : null;
    }

    @action add(record) {
        if (typeof record === 'object') {
            this.byId[record.id] = record;
            return true;
        }
        return false;
    }

    @action update(source) {
        const record = this.get(source);
        if (record) {
            if (typeof record.update === 'function')
                record.update(source);
            else
                merge(record, source);
        }
    }

    @action updateAll(source) {
        forEach(this.byId, record => {
            if (typeof record.update === 'function')
                record.update(source);
            else
                merge(record, source);
        });
    }

    has(recordOrId) {
        return Boolean(this.get(recordOrId));
    }

    @action upsert(record) {
        if (this.has(record)) {
            return this.update(record);
        } else {
            return this.add(record);
        }
    }

    @action delete(recordOrId) {
        if (this.has(recordOrId)) {
            const record = this.get(recordOrId);

            delete this.byId[record.id];
            return true;
        }
        return false;
    }
}
