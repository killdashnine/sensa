import {useEffect} from "react";
import {action} from "mobx";


export default function useSubscription(service) {
    useEffect(() => {
        service.subscribe();
        return () => {
            service.unsubscribe();
        };
    }, []);

    useEffect(() => {
        service.fetch().then(action(() => {
            service.initialized = true;
        }));
    }, service.deps || []);
}
