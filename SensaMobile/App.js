import React from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import TopAppBar from "./ui/TopAppBar/TopAppBar";
import BottomAppBar from "./ui/BottomAppBar/BottomAppBar";
import EventProvider from "./providers/EventProvider";
import EventList from "./ui/EventList/EventList";


export default function App() {
    return (<SafeAreaView style={styles.container}>
        <View style={styles.topAppBar}>
            <TopAppBar/>
        </View>
        <TopAppBar/>
        <EventProvider>
            <EventList BottomReserve={() => <View>
                <BottomAppBar/>
                <View style={styles.backgroundStub}/>
            </View>}/>
        </EventProvider>
        <View style={styles.bottomAppBar}>
            <BottomAppBar/>
        </View>
    </SafeAreaView>);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2B2A28',
    },
    topAppBar: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    bottomAppBar: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'transparent',
    },
    backgroundStub: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#2B2A28',
    },
});
